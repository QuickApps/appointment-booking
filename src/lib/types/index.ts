export interface Appointment {
	date: string;
	time: string;
	service: string;
	confirmed: boolean;
}

export interface Service {
	id: string;
	name: string;
	price: number;
	time: number;
}
