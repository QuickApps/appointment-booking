import { services } from '$lib/constants';
import type { Service } from '$lib/types';

export const formatDate = (date: string) => {
	const months: string[] = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	];

	const [year, month, day] = date.split('-');
	const formattedMonth = months[parseInt(month, 10) - 1];
	return `${formattedMonth} ${parseInt(day, 10)}, ${year}`;
};

export const getService = (id: string): Service => {
	let selectedIndex: number;
	services.some((val: Service, i) => {
		if (val.id === id) {
			selectedIndex = i;
			return true;
		}
	});

	return services[selectedIndex!];
};
