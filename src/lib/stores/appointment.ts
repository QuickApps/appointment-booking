import { browser } from '$app/environment';
import type { Appointment } from '$lib/types';
import { writable } from 'svelte/store';

let appointmentData: Appointment =
	browser &&
	(JSON.parse(localStorage.getItem('appointmentData') as string) || {
		date: '',
		time: '',
		service: '',
		confirmed: false
	});

export const appointment = writable(appointmentData);

appointment.subscribe(
	(val) => browser && localStorage.setItem('appointmentData', JSON.stringify(val))
);
