import type { Service } from '$lib/types';

export const services: Service[] = [
	{
		id: '1',
		name: 'Haircut',
		price: 20,
		time: 30
	},
	{
		id: '2',
		name: 'Beard groom',
		price: 15,
		time: 15
	},
	{
		id: '3',
		name: 'Fade',
		price: 25,
		time: 45
	},
	{
		id: '4',
		name: 'Lineup',
		price: 15,
		time: 10
	},
	{
		id: '5',
		name: 'Eyebrows',
		price: 10,
		time: 10
	}
];
