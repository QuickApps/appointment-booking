import { browser } from '$app/environment';
import { writable } from 'svelte/store';

let stepData: number = (browser && Number(localStorage.getItem('stepData'))) || 1;

export const step = writable(stepData);

step.subscribe((val) => browser && localStorage.setItem('stepData', val.toString()));
